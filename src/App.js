import './App.css';
import Menu from "./components/Menu";
import NoteList from "./components/NoteList";
import Header from "./components/Header";
import Main from "./components/Main";
import {useState, useEffect} from "react";
import { v4 as uuidv4 } from 'uuid'; // id randomizer! 🤙

// import logo from './assets/logo.png';
// import ReactDOM from 'react-dom';

// mui imports --- cleanup TBD ---
// import TagIcon from '@mui/icons-material/Tag';
// import CollectionsBookmarkIcon from '@mui/icons-material/CollectionsBookmark';
// import AccountCircleIcon from '@mui/icons-material/AccountCircle';
// import SortIcon from '@mui/icons-material/Sort';
// import ListIcon from '@mui/icons-material/List';
// import SortByAlphaIcon from '@mui/icons-material/SortByAlpha';
// import TextField from '@mui/material/TextField';
// import Chip from '@mui/material/Chip';
// import Divider from '@mui/material/Divider';
// import CssBaseline from '@mui/material/CssBaseline';



function App() {


  const [notes, setNotes] = useState(
    localStorage.notes ? JSON.parse(localStorage.notes) : []
  );


    // ============================= STORE TO LOCAL STORAGE
    useEffect(() => {
      // store in local storage with each change
      localStorage.setItem("storedNotes", JSON.stringify(notes))
    }, [notes]);

      // display active note state
    const [activeNote, setActiveNote] = useState(false)

      // ============================= ADD NOTE
    const addNote = () => {
    // make a note object
    const newNote = {
      id: uuidv4(),
      title: "",
      body: "",
      lastModified: Date.now(),
    };
      // append to current array with spread
      setNotes([newNote, ...notes]) // add newnote to existing notes
    }

      // ============================= EDIT NOTE
  const editNote = (editedNote) => {
    // use map to modify the array
    const editedNoteArr = notes.map((note) => {
      if(note.id === activeNote){
        // replace with edited note
        return editedNote
      }
      // otherwise return no edit
      return note
    })
    setNotes(editedNoteArr)
  }
  // ============================= DELETE NOTE
  const deleteNote = (idDelete) => {
    setNotes(notes.filter((note) => note.id !== idDelete ))
  }
  // ============================= NOTE -> MAIN DISPLAY
  const getActiveNote = () => {
    return notes.find((note) => note.id === activeNote );
  }

  return (
  <div className="App wrapper">
    <Menu/>
    <NoteList 
      notes={notes} 
      addNote={addNote}
      deleteNote={deleteNote}
      activeNote={activeNote}
      setActiveNote={setActiveNote}
    />
    <Header />
    <Main activeNote={getActiveNote()} editNote={editNote} />
  </div>
  );
}

export default App;


/*
mui example
import Button from '@mui/material/Button';
...
<Button variant="contained">Hello M-UI</Button>
*/